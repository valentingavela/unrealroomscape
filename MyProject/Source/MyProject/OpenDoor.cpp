// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "GameFramework/Actor.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
}

void UOpenDoor::OpenDoor()
{
	OnOpenRequest.Broadcast();
	////Find Owning actor
	//AActor* Owner = GetOwner();

	////Create Rotator
	////FRotator ActorRotation = GetOwner()->GetTransform().GetRotation().get;
	//FRotator NewRotation = FRotator(0.f, OpenAngle, 0.f);
	////Set the door rotation
	//Owner->SetActorRotation(NewRotation);
}

void UOpenDoor::CloseDoor()
{
	////Find Owning actor
	//AActor* Owner = GetOwner();

	////Create Rotator
	////FRotator ActorRotation = GetOwner()->GetTransform().GetRotation().get;
	//FRotator NewRotation = FRotator(0.f, 0.f, 0.f);
	////Set the door rotation
	//Owner->SetActorRotation(NewRotation);
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//realtimeSeconds = GetWorld()->GetRealTimeSeconds();

	if (PressurePlate->IsOverlappingActor(ActorThatOpens)) {
		OpenDoor();
	}
	else {
		CloseDoor();
	}

	

	// ...
}

